package DBPackage;

import Controller.LoginCtrl;
import Models.Tweet;
import Models.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mongodb.BasicDBObject;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Updates;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ManageData {
    MongoClient client;
    MongoDatabase database;
    ObjectMapper mapper;
    MongoCollection<Document> userCollection;
    MongoCollection<Document> tweetCollection;

    public ManageData() {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        this.client = client;
        this.mapper = new ObjectMapper();
        this.database = client.getDatabase("DBtest");
        this.userCollection = database.getCollection("Users");
        this.tweetCollection = database.getCollection("Tweets");
    }


    public void addFollowing(String id) throws JsonProcessingException {
        Document dc = userCollection.find(Filters.eq("userID", id)).first();
        dc.remove("_id");
        User user = mapper.readValue(dc.toJson(), User.class);

        if (!LoginCtrl.mainDoc.getFollowing().contains(id)) {
            LoginCtrl.mainDoc.getFollowing().add(id);
            userCollection.updateOne(Filters.eq("userID", LoginCtrl.mainDoc.getUserID()),
                    Updates.set("following", LoginCtrl.mainDoc.getFollowing()));
        }

        if (!user.getFollowers().contains(id)) {
            user.getFollowers().add(LoginCtrl.mainDoc.getUserID());
            userCollection.updateOne(Filters.eq("userID", user.getUserID()),
                    Updates.set("followers", user.getFollowers()));
        }
    }

    public void unFollowUser(String id) throws JsonProcessingException {
        Document dc = userCollection.find(Filters.eq("userID", id)).first();
        dc.remove("_id");
        User user = mapper.readValue(dc.toJson(), User.class);

        LoginCtrl.mainDoc.getFollowing().remove(id);
        userCollection.updateOne(Filters.eq("userID", LoginCtrl.mainDoc.getUserID()),
                Updates.set("following", LoginCtrl.mainDoc.getFollowing()));

        user.getFollowers().remove(LoginCtrl.mainDoc.getUserID());
        userCollection.updateOne(Filters.eq("userID", user.getUserID()),
                Updates.set("followers", user.getFollowers()));

    }


    public void deleteTweet(Tweet oldTweet) {
        Document document = tweetCollection.find(Filters.eq("tweetID", oldTweet.getTweetID())).first();
        tweetCollection.deleteOne(document);
    }

    public void addTweet(Tweet newTweet) throws JsonProcessingException {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        tweetCollection.createIndex(Indexes.ascending("tweetID"),
                new IndexOptions().unique(true));
        tweetCollection.insertOne(Document.parse(mapper.writeValueAsString(newTweet)));

    }


    public void addLiker(Tweet t) throws JsonProcessingException {
        Document dc = tweetCollection.find(Filters.eq("tweetID", t.getTweetID())).first();
        dc.remove("_id");
        Tweet tweet = mapper.readValue(dc.toJson(), Tweet.class);

        tweet.getLikes().add(LoginCtrl.mainDoc.getUserID());
        tweetCollection.updateOne(Filters.eq("tweetID", tweet.getTweetID()),
                Updates.set("likes", tweet.getLikes()));

        t.getLikes().add(LoginCtrl.mainDoc.getUserID());
    }

    public void deleteLiker(Tweet t) throws JsonProcessingException {
        Document dc = tweetCollection.find(Filters.eq("tweetID", t.getTweetID())).first();
        dc.remove("_id");
        Tweet tweet = mapper.readValue(dc.toJson(), Tweet.class);

        tweet.getLikes().remove(LoginCtrl.mainDoc.getUserID());
        tweetCollection.updateOne(Filters.eq("tweetID", tweet.getTweetID()),
                Updates.set("likes", tweet.getLikes()));

        t.getLikes().remove(LoginCtrl.mainDoc.getUserID());
    }


    public int getCountTweetColl() {
        Document d = tweetCollection.find().sort(new BasicDBObject("tweetID"
                , -1)).limit(1).first();
        if (d == null) {
            return 0;
        }
        return (int) d.get("tweetID");
    }

    public String getUserName(String id) {
        Document document = userCollection.find(Filters.eq("userID", id)).first();
        return (String) document.get("username");
    }

    public ArrayList<String> getFollowers(String mainUser) {
        Document document = userCollection.find(Filters.eq("userID", mainUser)).first();
        return (ArrayList<String>) document.get("followers");
    }

    public ArrayList<String> getFollowing(String mainUser) {
        Document document = userCollection.find(Filters.eq("userID", mainUser)).first();
        return (ArrayList<String>) document.get("following");
    }

    public User getUser(String id) throws JsonProcessingException {
        Document document = userCollection.find(Filters.eq("userID", id)).first();
        document.remove("_id");
        User user = mapper.readValue(document.toJson(), User.class);
        return user;
    }

    public ArrayList<Tweet> getAllTweets() throws JsonProcessingException {
        List<Document> docs = tweetCollection.find().into(new ArrayList<>());
        ArrayList<Tweet> tweetList = new ArrayList<>();

        for (Document d : docs) {
            d.remove("_id");
            tweetList.add(mapper.readValue(d.toJson(), Tweet.class));
        }

        return tweetList;
    }

    public ArrayList<Tweet> getUserTweets(User mainUser) throws JsonProcessingException {
        ArrayList<Tweet> list = new ArrayList<>();
        FindIterable<Document> iterDoc = tweetCollection.find(Filters.eq("senderID",
                mainUser.getUserID()));
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            Document itDoc = (Document) it.next();
            itDoc.remove("_id");
            list.add((mapper.readValue(itDoc.toJson(), Tweet.class)));
        }
        return list;
    }


    public void updateTweets(String id) throws JsonProcessingException {
        ArrayList<Tweet> tweets = getAllTweets();

        for (Tweet t : tweets) {
            for (String s : t.getLikes()) {
                if (id.equals(s)) {
                    t.getLikes().remove(id);
                    tweetCollection.updateOne(Filters.eq("tweetID", t.getTweetID()),
                            Updates.set("likes", t.getLikes()));
                }
            }
        }
    }

    public void updatePassword(String email, String pass) {
        userCollection.updateOne(Filters.eq("email", email),
                Updates.set("password", pass));
        LoginCtrl.mainDoc.setPassword(pass);
    }

    public void updateName(String name) throws JsonProcessingException {
        userCollection.updateOne(Filters.eq("userID", LoginCtrl.mainDoc.getUserID()),
                Updates.set("username", name));
        LoginCtrl.mainDoc.setUsername(name);
    }

    public void updateID(String id) throws JsonProcessingException {
        userCollection.updateOne(Filters.eq("userID", LoginCtrl.mainDoc.getUserID()),
                Updates.set("userID", id));
        LoginCtrl.mainDoc.setUserID(id);
    }

    public void updateEmail(String email) throws JsonProcessingException {
        userCollection.updateOne(Filters.eq("userID", LoginCtrl.mainDoc.getUserID()),
                Updates.set("email", email));
        LoginCtrl.mainDoc.setEmail(email);
    }

    public void updateBio(String bio) throws JsonProcessingException {
        userCollection.updateOne(Filters.eq("userID", LoginCtrl.mainDoc.getUserID()),
                Updates.set("bio", bio));
        LoginCtrl.mainDoc.setBio(bio);
    }


    public void deleteAccount(User u) throws JsonProcessingException {
        Document dc = userCollection.find(Filters.eq("userID", u.getUserID())).first();
        userCollection.deleteOne(dc);

        tweetCollection.deleteMany(Filters.eq("senderID",
                u.getUserID()));

        LoginCtrl.mainDoc = null;
    }
}
