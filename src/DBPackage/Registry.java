package DBPackage;


import Models.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.mindrot.jbcrypt.BCrypt;

import javax.mail.MessagingException;


public class Registry {
    MongoClient client;
    ObjectMapper mapper;
    MongoDatabase database;
    EmailHandling emailHandling;
    MongoCollection<Document> collection;
    public static boolean flag = false;

    public Registry() {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        this.client = client;
        this.mapper = new ObjectMapper();
        this.database = client.getDatabase("DBtest");
        this.collection = database.getCollection("Users");
        this.emailHandling = new EmailHandling();
    }

    public boolean LogIn(String ID, String pass) {
        Document dc = collection.find(Filters.eq("userID", ID)).first();
        if (dc == null || !checkPass(pass, (String) dc.get("password"))) {
            return false;
        }
        return true;
    }

    public boolean signUp(User user) throws MessagingException {
        Document dc = collection.find(Filters.eq("userID", user.getUserID())).first();
        Document dc1 = collection.find(Filters.eq("email", user.getEmail())).first();
        if (dc == null && dc1 == null) {
            emailHandling.sendEmail(user.getEmail());
            return true;
        } else if (dc == null) {
            flag = true;
            return false;
        }

        return false;
    }

    public User getUserFromDatabase(String ID) throws JsonProcessingException {
        Document document = collection.find(Filters.eq("userID", ID)).first();
        document.remove("_id");
        return mapper.readValue(document.toJson(), User.class);
    }


    public String searchFindPass(String email) {
        Document dc = collection.find(Filters.eq("email", email)).first();

        //generate random pass
        String newPass = String.valueOf(emailHandling.generateCode());

        //hash pass
        String hash = new User().hashPass(newPass);
        collection.updateOne(dc, Updates.set("password", hash));

        return newPass;
    }

    public boolean searchFindEmail(String email) {
        Document dc = collection.find(Filters.eq("email", email)).first();
        return dc == null ? false : true;
    }


    public boolean checkPass(String plainPassword, String hashedPassword) {
        if (BCrypt.checkpw(plainPassword, hashedPassword))
            return true;

        return false;
    }

    public boolean checkEnterCode(User user, String code) throws JsonProcessingException {
        int submittedCode = emailHandling.emailCodeMap.get(user.getEmail());

        if (emailHandling.emailCodeMap.containsKey(user.getEmail())
                && submittedCode == Integer.parseInt(code)) {
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            collection.createIndex(Indexes.ascending("userID", "email"),
                    new IndexOptions().unique(true));
            collection.insertOne(Document.parse(mapper.writeValueAsString(user)));
            return true;
        }
        return false;
    }
}
