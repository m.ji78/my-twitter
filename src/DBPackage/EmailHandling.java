package DBPackage;

import Controller.EnterCodeCtrl;
import Controller.LoginCtrl;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

public class EmailHandling {
    Map<String, Integer> emailCodeMap;
    private Properties properties;
    private MimeMessage message;
    private final String HOST;
    private final String PORT;
    private Session session;

    public EmailHandling() {
        this.emailCodeMap = new HashMap<>();
        this.properties = System.getProperties();
        this.HOST = "smtp.gmail.com";
        this.PORT = "465";
    }

    public void sendEmailToRecoverPassword(String email) throws MessagingException {
        setP();

        message = new MimeMessage(session);
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
        message.setSubject("MyTwitter\n\n");
        String rePass = LoginCtrl.registry.searchFindPass(email);
        message.setText("Dear " + email + "," +
                " \nYour Password is: " + rePass);

        Transport.send(message);
    }

    public void sendEmail(String email) throws MessagingException {
        setP();

        int code = generateCode();
        emailCodeMap.put(email, code);

        message = new MimeMessage(session);
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
        message.setSubject("MyTwitter\n\n");
        message.setText("Hello " + EnterCodeCtrl.userInEnterCode.getUsername() + "," +
                " \nYour verification code is: " + code);
        Transport.send(message);
    }

    public int generateCode() {
        Random random = new Random();
        return Math.abs(random.nextInt(9000) + 1000);
    }

    private void setP() {
        properties.setProperty("mail.smtp.host", HOST);
        properties.setProperty("mail.smtp.port", PORT);
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.ssl.enable", "true");
        properties.setProperty("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");

        session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("mytwitter.app2021@gmail.com",
                        "123456789mytwitter");
            }
        });
    }
}