package Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;



@JsonIgnoreProperties(ignoreUnknown = true)
public class Tweet {
    private String date;
    private String post;
    private long tweetID;
    private String senderID;
    private String senderUsrName;
    private ArrayList<String> likes;

    public Tweet() {
    }

    public Tweet(long tweetID, String date, String post, String senderID, String senderUsrName) {
        this.post = post;
        this.date = date;
        this.tweetID = tweetID;
        this.senderID = senderID;
        this.likes = new ArrayList<>();
        this.senderUsrName = senderUsrName;
    }

    public String getDate() {
        return date;
    }

    public String getPost() {
        return post;
    }

    public long getTweetID() {
        return tweetID;
    }

    public String getSenderID() {
        return senderID;
    }

    public ArrayList<String> getLikes() {
        return likes;
    }

    public String getSenderUsrName() {
        return senderUsrName;
    }

}
