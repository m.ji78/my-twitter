package Models;

public class Values {

    public static String ErrorString(int num) {
        switch (num) {
            case 1:
                return "ID or password is incorrect";
            case 2:
                return "This ID has already been used";
            case 3:
                return "This Email has already been used";
            case 4:
                return "Password must contains 4 characters";
            case 5:
                return "The entered code is incorrect";
            case 6:
                return "The new password and confirmation password don't match ";
            case 7:
                return "The current password is not incorrect";
            case 8:
                return "Your Email address is not incorrect";
            case 9:
                return "Account successfully created.";
            case 10:
                return "Please fill all fields";
            case 11:
                return "Password successfully changed";
            case 12:
                return "The entered password is not incorrect";
        }
        return null;
    }

    public static final int USER_PASS_INCORRECT = 1;
    public static final int USERNAME_USED = 2;
    public static final int EMAIL_USED = 3;
    public static final int PASS_4_CHAR = 4;
    public static final int INCORRECT_CODE = 5;
    public static final int CHANGE_PASS_ERROR = 6;
    public static final int OLD_PASS_INCORRECT = 7;
    public static final int RESET_PASS_ERROR = 8;
    public static final int SIGN_UP = 9;
    public static final int FILL_FIELDS = 10;
    public static final int PASS_CHANGED = 11;
    public static final int PASS_INCORRECT = 12;
}
