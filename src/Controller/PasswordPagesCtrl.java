package Controller;


import DBPackage.EmailHandling;
import Models.Values;
import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import javax.mail.MessagingException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PasswordPagesCtrl implements Initializable {

    @FXML
    public Pane resetPasswordPage;

    @FXML
    public StackPane stack;

    @FXML
    public JFXButton resetButton;

    @FXML
    public TextField emailField;

    @FXML
    public Label error;


    @FXML
    public void minimizeHandler(MouseEvent event) {
        Main.pStage.setIconified(true);
    }

    @FXML
    public void closeHandler(MouseEvent event) {
        Main.pStage.close();
    }

    @FXML
    public void undoInResetPageHandler(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../View/Login.fxml"));
        stack.getChildren().setAll(root);
    }

    @FXML
    public void resetButtonHandler(ActionEvent e) {
        boolean result = LoginCtrl.registry.searchFindEmail(emailField.getText());

        if (result) {
            EmailHandling eh = new EmailHandling();
            try {
                eh.sendEmailToRecoverPassword(emailField.getText());
                emailField.setText("");
            } catch (MessagingException messagingException) {
                messagingException.printStackTrace();
            }
        } else {
            error.setText(Values.ErrorString(Values.RESET_PASS_ERROR));
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        error.setTextFill(Color.rgb(144, 8, 8));
    }
}
