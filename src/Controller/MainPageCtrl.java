package Controller;

import DBPackage.ManageData;
import Models.Tweet;
import Models.User;
import Models.Values;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.IntStream;


public class MainPageCtrl implements Initializable {
    public static ManageData manageData;
    private String userPageID;
    private String pageState;
    public static Long tId;
    //todo: search


    public MainPageCtrl() {
        manageData = new ManageData();
        this.pageState = "";
    }

    @FXML
    public VBox mainP;

    @FXML
    private TextField SearchField;

    @FXML
    private Label searchInMainPage;

    @FXML
    private VBox listPage;

    @FXML
    private Pane profilePage;

    @FXML
    private TextField usernameTextF;

    @FXML
    private TextField emailTextF;

    @FXML
    private TextField IDTextF;

    @FXML
    private TextArea bio;

    @FXML
    private Pane changePasswordPage;

    @FXML
    private PasswordField cunPass;

    @FXML
    private PasswordField newPass;

    @FXML
    private PasswordField confirmPass;

    @FXML
    private Pane goToPage;

    @FXML
    private ScrollPane showTweetsPage;

    @FXML
    private ScrollPane scrollPaneForShowFollower;

    @FXML
    private ScrollPane scrollPaneForShowFollowing;

    @FXML
    private VBox tweetsPage;

    @FXML
    private HBox showFriendsPage;

    @FXML
    private TextField SearchFollowing;

    @FXML
    private VBox VBoxForFollower;

    @FXML
    private TextField SearchFollowers;

    @FXML
    private VBox VBoxForFollowing;

    @FXML
    private Label errorLabelInChangePassPage;

    @FXML
    private StackPane stackPane;

    @FXML
    private StackPane stackForDeletePage;

    @FXML
    private Pane deleteAccountPage;

    @FXML
    private Label errorInDeletePage;

    @FXML
    private JFXButton editButton;

    @FXML
    private ScrollPane UserPage;

    @FXML
    private VBox VBoxForUserPage;

    @FXML
    private VBox VBoxForShowLikers;

    @FXML
    private Circle circleForUserImage;

    @FXML
    private Label usernameLabelInUserPage;

    @FXML
    private Label IDLabelInUserPage;

    @FXML
    private ScrollPane showLikersPane;

    @FXML
    private Label numberOfFollowing;

    @FXML
    private Label numberOfFollowers;

    @FXML
    private Pane paneInUserPage;

    @FXML
    private PasswordField passInDeleteAcc;

    @FXML
    private TextArea bioInUserPage;

    @FXML
    private JFXButton fuBtnInUserPage;

    @FXML
    private TextArea addTweetTextArea;


    public void startPage() throws JsonProcessingException {
        pageState = "MainPage";
        prepareMainPage();
    }


    @FXML
    void profileButtonHandler(MouseEvent event) {
        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(true);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);

        setInfo();
    }

    @FXML
    void EditInfo(ActionEvent event) {
        editButton.setText("Save");
        String oldUsername, oldID, oldBio, oldEmail;
        oldUsername = usernameTextF.getText();
        oldEmail = emailTextF.getText();
        oldID = IDTextF.getText();
        oldBio = bio.getText();

        usernameTextF.setEditable(true);
        emailTextF.setEditable(true);
        IDTextF.setEditable(true);
        bio.setEditable(true);

        editButton.setOnAction(e -> {
            if (!usernameTextF.getText().equals(oldUsername)) {
                try {
                    manageData.updateName(usernameTextF.getText());
                } catch (JsonProcessingException jsonProcessingException) {
                    jsonProcessingException.printStackTrace();
                }
            }

            if (!IDTextF.getText().equals(oldID)) {
                try {
                    manageData.updateID(IDTextF.getText());
                } catch (JsonProcessingException jsonProcessingException) {
                    jsonProcessingException.printStackTrace();
                }
            }

            if (!emailTextF.getText().equals(oldEmail)) {
                try {
                    manageData.updateEmail(emailTextF.getText());
                } catch (JsonProcessingException jsonProcessingException) {
                    jsonProcessingException.printStackTrace();
                }
            }

            if (!bio.getText().equals(oldBio) || bio.equals(null)) {
                try {
                    manageData.updateBio(bio.getText());
                } catch (JsonProcessingException jsonProcessingException) {
                    jsonProcessingException.printStackTrace();
                }
            }

            editButton.setText("Edit");

            usernameTextF.setEditable(false);
            emailTextF.setEditable(false);
            IDTextF.setEditable(false);
            bio.setEditable(false);

        });

    }

    @FXML
    void changePassLabel(MouseEvent event) {
        errorLabelInChangePassPage.setText("");
        confirmPass.setText("");
        cunPass.setText("");
        newPass.setText("");

        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(true);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);

    }

    @FXML
    void changeHandler(ActionEvent event) {
        if (LoginCtrl.registry.checkPass(cunPass.getText(), LoginCtrl.mainDoc.getPassword())) {
            if (newPass.getText().length() != 4) {
                errorLabelInChangePassPage.setText(Values.ErrorString(Values.PASS_4_CHAR));
            } else {
                if (confirmPass.getText().equals(newPass.getText())) {
                    manageData.updatePassword(LoginCtrl.mainDoc.getEmail(), LoginCtrl.mainDoc.hashPass(newPass.getText()));
                    //errorLabelInChangePassPage.setTextFill(Color.rgb(0, 153, 0));
                    //errorLabelInChangePassPage.setText(Values.ErrorString(Values.PASS_CHANGED));
                    //todo  handler :)

                    mainP.setVisible(false);
                    showFriendsPage.setVisible(false);
                    showTweetsPage.setVisible(false);
                    goToPage.setVisible(false);
                    profilePage.setVisible(true);
                    changePasswordPage.setVisible(false);
                    stackForDeletePage.setVisible(false);
                    deleteAccountPage.setVisible(false);
                } else {
                    errorLabelInChangePassPage.setFont(new Font(12));
                    errorLabelInChangePassPage.setText(Values.ErrorString(Values.CHANGE_PASS_ERROR));
                    errorLabelInChangePassPage.setFont(new Font(13));
                }
            }
        } else {
            errorLabelInChangePassPage.setText(Values.ErrorString(Values.OLD_PASS_INCORRECT));
        }
    }


    @FXML
    void myFriendsButtonHandler(ActionEvent event) {
        prepareFriendsPage();

        mainP.setVisible(false);
        showFriendsPage.setVisible(true);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);
    }

    @FXML
    void myTweetsButtonHandler(ActionEvent event) throws JsonProcessingException {
        pageState = "MyTweet";
        PrepareMyTweetsPage();

        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(true);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);
    }

    @FXML
    void addTweetHandler(ActionEvent event) throws JsonProcessingException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date d = new Date();

        tId = (long) manageData.getCountTweetColl() + 1;

        if (!addTweetTextArea.getText().equals("")) {
            Tweet tweet = new Tweet(tId, formatter.format(d),
                    addTweetTextArea.getText(),
                    LoginCtrl.mainDoc.getUserID(),
                    LoginCtrl.mainDoc.getUsername());

            addTweetTextArea.setText("");

            Pane pane = makePaneForTweet(tweet, true);
            tweetsPage.getChildren().add(pane);
            manageData.addTweet(tweet);
        }
    }

    @FXML
    void logoutHandler(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../View/Login.fxml"));
        stackPane.getChildren().setAll(root);
    }

    @FXML
    void moreActionHandler(MouseEvent e) {
        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(true);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);

    }


    @FXML
    void undoInFriendsPage(MouseEvent event) {
        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(true);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);


    }

    @FXML
    void undoInProfilePage(MouseEvent event) {
        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(true);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);

    }

    @FXML
    void undoInMyTweetsPage(MouseEvent event) {
        tweetsPage.getChildren().removeAll(tweetsPage.getChildren());

        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(true);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);

    }

    @FXML
    void undoInDeleteAccountPage(MouseEvent event) {
        passInDeleteAcc.setText("");

        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(true);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);
    }

    @FXML
    void undoInChangePassPageHandler(MouseEvent event) {
        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(true);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);

        newPass.setText("");
        cunPass.setText("");
        confirmPass.setText("");
        errorLabelInChangePassPage.setText("");

    }

    @FXML
    void undoInGoToPage(MouseEvent event) throws JsonProcessingException {
        prepareMainPage();
        pageState = "MainPage";

        mainP.setVisible(true);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);
    }

    @FXML
    void undoInUserPage(MouseEvent event) throws JsonProcessingException {
        prepareMainPage();

        mainP.setVisible(true);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);
    }

    @FXML
    void undoInLikersPage(MouseEvent event) throws JsonProcessingException {
        VBoxForShowLikers.getChildren().removeAll(VBoxForShowLikers.getChildren());

        switch (pageState) {
            case "MyTweet": {
                tweetsPage.getChildren().removeAll(tweetsPage.getChildren());
                PrepareMyTweetsPage();

                mainP.setVisible(false);
                showFriendsPage.setVisible(false);
                showTweetsPage.setVisible(true);
                goToPage.setVisible(false);
                profilePage.setVisible(false);
                changePasswordPage.setVisible(false);
                stackForDeletePage.setVisible(false);
                deleteAccountPage.setVisible(false);
                UserPage.setVisible(false);
                showLikersPane.setVisible(false);

                break;

            }
            case "MainPage": {
                mainP.getChildren().removeAll(tweetsPage.getChildren());
                prepareMainPage();

                mainP.setVisible(true);
                showFriendsPage.setVisible(false);
                showTweetsPage.setVisible(false);
                goToPage.setVisible(false);
                profilePage.setVisible(false);
                changePasswordPage.setVisible(false);
                stackForDeletePage.setVisible(false);
                deleteAccountPage.setVisible(false);
                UserPage.setVisible(false);
                showLikersPane.setVisible(false);

                break;

            }

            case "userPage": {
                prepareUserPage(userPageID);

                mainP.setVisible(false);
                showFriendsPage.setVisible(false);
                showTweetsPage.setVisible(false);
                goToPage.setVisible(false);
                profilePage.setVisible(false);
                changePasswordPage.setVisible(false);
                stackForDeletePage.setVisible(false);
                deleteAccountPage.setVisible(false);
                UserPage.setVisible(true);
                showLikersPane.setVisible(false);

                break;

            }
        }

    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        errorInDeletePage.setTextFill(Color.rgb(144, 8, 8));
        errorLabelInChangePassPage.setTextFill(Color.rgb(144, 8, 8));

        mainP.setVisible(true);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        deleteAccountPage.setVisible(false);
        stackForDeletePage.setDisable(false);
        showLikersPane.setVisible(false);
        UserPage.setVisible(false);

        scrollPaneForShowFollower.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
        VBoxForFollower.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                scrollPaneForShowFollower.vbarPolicyProperty().
                        setValue(ScrollPane.ScrollBarPolicy.AS_NEEDED);
            }
        });

        VBoxForFollower.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                scrollPaneForShowFollower.vbarPolicyProperty().
                        setValue(ScrollPane.ScrollBarPolicy.NEVER);
            }
        });

        scrollPaneForShowFollowing.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
        VBoxForFollowing.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                scrollPaneForShowFollowing.vbarPolicyProperty().
                        setValue(ScrollPane.ScrollBarPolicy.AS_NEEDED);
            }
        });

        VBoxForFollowing.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                scrollPaneForShowFollowing.vbarPolicyProperty().
                        setValue(ScrollPane.ScrollBarPolicy.NEVER);
            }
        });


        newPass.textProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue == null) {
                return;
            }

            if (newValue.length() > 4) {
                newPass.setText(oldValue);
            } else {
                newPass.setText(newValue);
            }
        });

        confirmPass.textProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue == null) {
                return;
            }

            if (newValue.length() > 4) {
                confirmPass.setText(oldValue);
            } else {
                confirmPass.setText(newValue);
            }
        });
    }


    public void makeAddTweetPane() {
        ImageView imageView = new ImageView();
        imageView.setFitWidth(25);
        imageView.setFitHeight(25);
        imageView.setImage(new Image(new File("src/Images/undo-variant.png")
                .toURI().toString()));
        Label undo = new Label("", imageView);
        undo.setLayoutX(0);
        undo.setLayoutY(0);
        undo.getStyleClass().add("back-btn");
        undo.setOnMouseClicked(event -> {
            undoInMyTweetsPage(event);
        });

        tweetsPage.getChildren().add(undo);

        Pane pane = new Pane();
        pane.setPrefWidth(420);
        pane.setPrefHeight(180);

        TextArea textArea = new TextArea();
        textArea.setPrefWidth(420);
        textArea.setPrefHeight(120);
        textArea.setLayoutX(0);
        textArea.setLayoutY(7);
        textArea.setStyle("-fx-background-color: #ffffff");
        textArea.setBlendMode(BlendMode.DARKEN);
        addTweetTextArea = textArea;
        textArea.setWrapText(true);


        JFXButton button = new JFXButton("Add Tweet");
        button.setPrefWidth(110);
        button.setPrefHeight(26);
        button.setLayoutX(155);
        button.setLayoutY(142);
        button.setStyle("-fx-background-color:  #7f78d2");
        button.setTextFill(Color.rgb(34, 31, 59));
        button.setOnAction(e -> {
            try {
                addTweetHandler(e);
            } catch (JsonProcessingException jsonProcessingException) {
                jsonProcessingException.printStackTrace();
            }
        });

        pane.getChildren().addAll(textArea, button);
        tweetsPage.getChildren().add(pane);

    }

    private void prepareFriendsPage() {
        ArrayList<String> followerUsers;
        ArrayList<String> followingUsers;
        followerUsers = manageData.getFollowers(LoginCtrl.mainDoc.getUserID());
        followingUsers = manageData.getFollowing(LoginCtrl.mainDoc.getUserID());

        Pane pane;
        VBoxForFollowing.getChildren().removeAll(VBoxForFollowing.getChildren());
        VBoxForFollower.getChildren().removeAll(VBoxForFollower.getChildren());
        if (followerUsers.isEmpty() || followingUsers.isEmpty()) {
            scrollPaneForShowFollower.setVisible(false);
            scrollPaneForShowFollowing.setVisible(false);
        } else {
            for (String u : followerUsers) {
                pane = makePaneForFriends(u);
                VBoxForFollower.getChildren().add(pane);
                pane.setOnMouseClicked(event -> {
                    try {
                        friendsPaneAction(u);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                });
            }

            for (String u : followingUsers) {
                pane = makePaneForFriends(u);
                VBoxForFollowing.getChildren().add(pane);
                pane.setOnMouseClicked(event -> {
                    try {
                        friendsPaneAction(u);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                });
            }
        }

    }

    private Pane makePaneForLiker(User u) {
        Pane pane = new Pane();
        pane.setPrefWidth(420);
        pane.setPrefHeight(80);
        pane.setOnMouseClicked(event -> {
            try {
                prepareUserPage(u.getUserID());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });

        Label usr = new Label(u.getUsername());
        usr.getStyleClass().add("label-btn");
        usr.setPrefWidth(180);
        usr.setPrefHeight(20);
        usr.setLayoutX(120);
        usr.setLayoutY(15);
        usr.setStyle("-fx-text-fill: #221f3b");

        Label usrID = new Label("@" + u.getUserID());
        usrID.setFont(new Font(13));
        usrID.setPrefWidth(180);
        usrID.setPrefHeight(20);
        usrID.setLayoutX(120);
        usrID.setLayoutY(45);
        usrID.setStyle("-fx-text-fill: #221f3b");

        Image image = new Image(new File("src/Images/avatar_people_person_profile_user_woman_icon_123358.png").toURI().toString());
        Circle circle = new Circle(35);
        circle.setCenterX(65);
        circle.setCenterY(40);
        ImagePattern imagePattern = new ImagePattern(image);
        circle.setFill(imagePattern);

        pane.getStyleClass().add("pane-btn");
        pane.getChildren().addAll(usr, usrID, circle);

        return pane;
    }

    private Pane makePaneForFriends(String id) {
        Pane pane = new Pane();
        pane.setPrefWidth(210);
        pane.setPrefHeight(60);

        Label usr = new Label(manageData.getUserName(id));
        usr.setFont(new Font(13));
        usr.setStyle("-fx-font-weight: BOLD");
        usr.setPrefWidth(120);
        usr.setPrefHeight(20);
        usr.setLayoutX(75);
        usr.setLayoutY(10);
        usr.setStyle("-fx-text-fill: #221f3b");

        Label usrID = new Label("@" + id);
        usrID.setFont(new Font(11));
        usrID.setPrefWidth(120);
        usrID.setPrefHeight(15);
        usrID.setLayoutX(75);
        usrID.setLayoutY(35);
        usrID.setStyle("-fx-text-fill: #221f3b");

        Image image = new Image(new File("src/Images/avatar_people_person_profile_user_woman_icon_123358.png").toURI().toString());
        Circle circle = new Circle(25);
        circle.setCenterX(40);
        circle.setCenterY(30);
        ImagePattern imagePattern = new ImagePattern(image);
        circle.setFill(imagePattern);

        pane.getStyleClass().add("pane-btn");
        pane.getChildren().addAll(usr, usrID, circle);

        return pane;
    }

    private Pane makePaneForTweet(Tweet t, boolean state) {
        Pane pane = new Pane();
        pane.setPrefWidth(420);

        Label numberOfLikes = new Label();
        numberOfLikes.setLayoutX(75);
        numberOfLikes.setLayoutY(210);
        numberOfLikes.setPrefWidth(90);
        numberOfLikes.setPrefHeight(16);
        numberOfLikes.setCursor(Cursor.HAND);
        numberOfLikes.setText(String.valueOf(t.getLikes().size()));
        numberOfLikes.setOnMouseClicked(event -> {
            try {
                VBoxForShowLikers.getChildren().removeAll(VBoxForShowLikers.getChildren());
                prepareLikerPane(t);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });

        Label date = new Label();
        date.setLayoutX(270);
        date.setLayoutY(205);
        date.setPrefWidth(170);
        date.setPrefHeight(25);
        date.setText(t.getDate());


        Label name = new Label();
        name.setLayoutX(100);
        name.setLayoutY(16);
        name.setPrefWidth(180);
        name.setPrefHeight(25);
        name.setText(t.getSenderUsrName());
        name.getStyleClass().add("label-btn");
        name.setStyle("-fx-text-fill: #221f3b");


        Label id = new Label();
        id.setLayoutX(100);
        id.setLayoutY(40);
        id.setPrefWidth(180);
        id.setPrefHeight(25);
        id.setCursor(Cursor.HAND);
        id.setStyle("-fx-text-fill: #221f3b");
        id.setText("@" + t.getSenderID());
        id.setOnMouseClicked(event -> {
            try {
                prepareUserPage(t.getSenderID());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });


        TextArea post = new TextArea();
        post.setLayoutX(27);
        post.setLayoutY(80);
        post.setPrefWidth(375);
        post.setPrefHeight(120);
        post.setWrapText(true);
        post.setEditable(false);
        post.setText(t.getPost());
        post.setFont(new Font(14));
        post.setStyle("-fx-background-color: #ffffff");


        ImageView imageView = new ImageView();
        imageView.setFitWidth(26);
        imageView.setFitHeight(26);
        imageView = setImageOfLike(checkLike(t), imageView);
        Label like = new Label("", imageView);
        like.setLayoutX(40);
        like.setLayoutY(205);
        ImageView finalImageView = imageView;
        like.setOnMouseClicked(event -> {
            try {
                checkImageForLike(finalImageView, t);
                numberOfLikes.setText(String.valueOf(t.getLikes().size()));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });


        Image image = randomImage();
        Circle circle = new Circle(28);
        circle.setCenterX(54);
        circle.setCenterY(44);
        circle.setFill(Color.valueOf("#b375ae"));
        ImagePattern imagePattern = new ImagePattern(image);
        circle.setFill(imagePattern);


        if (state) {
            MenuBar menuB = new MenuBar();
            menuB.setLayoutX(360);
            menuB.setLayoutY(10);
            menuB.setBlendMode(BlendMode.DARKEN);
            menuB.getStyleClass().add("menu-bar");


            ImageView userView = new ImageView(new Image(new File("src/Images/dots-vertical.png")
                    .toURI().toString()));
            userView.setFitWidth(20);
            userView.setFitHeight(20);

            Menu userMenu = new Menu();
            userMenu.setStyle("-fx-background-color: #ffffff");

            userMenu.setGraphic(userView);
            MenuItem item = new MenuItem("Delete");
            item.setOnAction(event -> {
                try {
                    deleteTweet(event, t);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            });


            userMenu.getItems().add(item);
            menuB.getMenus().add(userMenu);
            menuB.getStyleClass().add("menu-bar");

            pane.getChildren().addAll(menuB);

        }


        pane.getChildren().addAll(like, post, id, name, date, circle, numberOfLikes);

        return pane;
    }

    private void prepareMainPage() throws JsonProcessingException {
        ArrayList<Tweet> tweetList = manageData.getAllTweets();

        listPage.getChildren().removeAll(listPage.getChildren());

        if (tweetList != null) {
            Pane pane;

            Collections.reverse(tweetList);

            for (Tweet t : tweetList) {
                pane = makePaneForTweet(t, false);

                if (!t.getSenderID().equals(LoginCtrl.mainDoc.getUserID())) {
                    JFXButton btn = setStyleOnBtn(t);

                    btn.setOnAction(e -> {
                        try {
                            followUnfollowHandler(t, btn.getText(), btn);
                        } catch (JsonProcessingException jsonProcessingException) {
                            jsonProcessingException.printStackTrace();
                        }
                    });
                    pane.getChildren().add(btn);
                }
                listPage.getChildren().add(pane);
            }
        }
    }

    private void PrepareMyTweetsPage() throws JsonProcessingException {
        ArrayList<Tweet> listOfTweets = manageData.getUserTweets(LoginCtrl.mainDoc);
        makeAddTweetPane();

        Collections.reverse(listOfTweets);

        if (listOfTweets != null) {
            for (Tweet t : listOfTweets) {
                Pane pane = makePaneForTweet(t, true);
                tweetsPage.getChildren().add(pane);
            }
        }
    }

    private void prepareLikerPane(Tweet t) throws JsonProcessingException {
        ImageView imageView = new ImageView();
        imageView.setFitWidth(25);
        imageView.setFitHeight(25);
        imageView.setImage(new Image(new File("src/Images/undo-variant.png")
                .toURI().toString()));
        Label undo = new Label("", imageView);
        undo.setLayoutX(0);
        undo.setLayoutY(0);
        undo.getStyleClass().add("back-btn");
        undo.setOnMouseClicked(event -> {
            try {
                undoInLikersPage(event);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        VBoxForShowLikers.getChildren().add(undo);


        for (String u : t.getLikes()) {
            Pane pane = makePaneForLiker(manageData.getUser(u));
            VBoxForShowLikers.getChildren().add(pane);
        }


        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(true);

    }

    private void prepareUserPage(String id) throws JsonProcessingException {
        pageState = "userPage";
        userPageID = id;

        int i = VBoxForUserPage.getChildren().size();
        while (VBoxForUserPage.getChildren().size() != 3) {
            VBoxForUserPage.getChildren().remove(i - 1);
            i--;
        }

        User user = manageData.getUser(id);
        ArrayList<Tweet> tweets = manageData.getUserTweets(user);

        setInfoInUserPage(user);

        Collections.reverse(tweets);
        for (Tweet t : tweets) {
            Pane pane = makePaneForTweet(t, false);
            VBoxForUserPage.getChildren().add(pane);
        }

        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(true);
        showLikersPane.setVisible(false);
    }

    private void setInfoInUserPage(User user) {
        numberOfFollowing.setText(String.valueOf(user.getFollowing().size()));
        numberOfFollowing.setCursor(Cursor.HAND);
        numberOfFollowing.setOnMouseClicked(event -> {
            try {
                prepareShowFollowing(user.getUserID());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        numberOfFollowers.setText(String.valueOf(user.getFollowers().size()));
        numberOfFollowers.setCursor(Cursor.HAND);
        numberOfFollowers.setOnMouseClicked(event -> {
            try {
                prepareShowFollowers(user.getUserID());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        IDLabelInUserPage.setText("@" + user.getUserID());
        usernameLabelInUserPage.setText(user.getUsername());
        bioInUserPage.setText(user.getBio());

        if (!user.getUserID().equals(LoginCtrl.mainDoc.getUserID())) {
            fuHandler(user);
            fuBtnInUserPage.setVisible(true);
        } else {
            fuBtnInUserPage.setVisible(false);
        }
    }

    private void prepareShowFollowers(String user) throws JsonProcessingException {
        VBoxForShowLikers.getChildren().removeAll(VBoxForShowLikers.getChildren());

        ImageView imageView = new ImageView();
        imageView.setFitWidth(25);
        imageView.setFitHeight(25);
        imageView.setImage(new Image(new File("src/Images/undo-variant.png")
                .toURI().toString()));
        Label undo = new Label("", imageView);
        undo.setLayoutX(0);
        undo.setLayoutY(0);
        undo.getStyleClass().add("back-btn");
        undo.setOnMouseClicked(event -> {
            try {
                undoInLikersPage(event);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        VBoxForShowLikers.getChildren().add(undo);

        ArrayList<String> followersIDs = manageData.getFollowers(user);


        for (String s : followersIDs) {
            User u = manageData.getUser(s);
            Pane pane = makePaneForLiker(u);
            VBoxForShowLikers.getChildren().add(pane);
        }

        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(true);
    }

    private void prepareShowFollowing(String user) throws JsonProcessingException {
        VBoxForShowLikers.getChildren().removeAll(VBoxForShowLikers.getChildren());

        ImageView imageView = new ImageView();
        imageView.setFitWidth(25);
        imageView.setFitHeight(25);
        imageView.setImage(new Image(new File("src/Images/undo-variant.png")
                .toURI().toString()));
        Label undo = new Label("", imageView);
        undo.setLayoutX(0);
        undo.setLayoutY(0);
        undo.getStyleClass().add("back-btn");
        undo.setOnMouseClicked(event -> {
            try {
                undoInLikersPage(event);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        VBoxForShowLikers.getChildren().add(undo);

        ArrayList<String> followingIDs = manageData.getFollowing(user);


        for (String s : followingIDs) {
            User u = manageData.getUser(s);
            Pane pane = makePaneForLiker(u);
            VBoxForShowLikers.getChildren().add(pane);
        }

        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(false);
        deleteAccountPage.setVisible(false);
        UserPage.setVisible(false);
        showLikersPane.setVisible(true);

    }


    private void setInfo() {
        usernameTextF.setText(LoginCtrl.mainDoc.getUsername());
        IDTextF.setText(LoginCtrl.mainDoc.getUserID());
        emailTextF.setText(LoginCtrl.mainDoc.getEmail());
        bio.setText(LoginCtrl.mainDoc.getBio());
    }

    private Image randomImage() {
        Random rand = new Random();
        ArrayList<Image> arrs = new ArrayList<>();

        Image image0 = new Image(new File("src/Images/photo_2021-04-05_21-57-48.jpg").toURI().toString());
        Image image1 = new Image(new File("src/Images/photo_2021-04-05_23-13-06.jpg").toURI().toString());
        Image image2 = new Image(new File("src/Images/photo_2021-04-05_23-13-38.jpg").toURI().toString());
        Image image3 = new Image(new File("src/Images/photo_2021-04-05_23-13-45.jpg").toURI().toString());
        Image image4 = new Image(new File("src/Images/photo_2021-04-05_23-13-53.jpg").toURI().toString());
        Image image5 = new Image(new File("src/Images/photo_2021-04-05_23-14-08.jpg").toURI().toString());
        Image image6 = new Image(new File("src/Images/photo_2021-04-05_23-14-34.jpg").toURI().toString());

        arrs.add(image0);
        arrs.add(image1);
        arrs.add(image2);
        arrs.add(image3);
        arrs.add(image4);
        arrs.add(image5);
        arrs.add(image6);

        int n = Math.abs(rand.nextInt(7));
        return arrs.get(n);

    }

    private void fuHandler(User user) {
        if (LoginCtrl.mainDoc.getFollowing().contains(user.getUserID())) {
            fuBtnInUserPage.setText("UnFollow");
            fuBtnInUserPage.getStyleClass().add("unfollow-btn");
            fuBtnInUserPage.setStyle("-fx-background-color: #4d4c7d");
        } else {
            fuBtnInUserPage.setText("Follow");
            fuBtnInUserPage.getStyleClass().add("follow-btn");
            fuBtnInUserPage.setStyle("-fx-background-color: #7f78d2");
        }

        fuBtnInUserPage.setOnMouseClicked(event -> {
            if (fuBtnInUserPage.getText().equals("Follow")) {
                try {
                    manageData.addFollowing(user.getUserID());
                    fuBtnInUserPage.setText("UnFollow");
                    fuBtnInUserPage.getStyleClass().add("unfollow-btn");
                    fuBtnInUserPage.setStyle("-fx-background-color: #4d4c7d");
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }

            } else if (fuBtnInUserPage.getText().equals("UnFollow")) {

                try {
                    manageData.unFollowUser(user.getUserID());
                    fuBtnInUserPage.setText("Follow");
                    fuBtnInUserPage.getStyleClass().add("follow-btn");
                    fuBtnInUserPage.setStyle("-fx-background-color: #7f78d2");
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean checkLike(Tweet tweet) {
        if (tweet.getLikes().contains(LoginCtrl.mainDoc.getUserID())) {
            return true;
        }
        return false;
    }

    private JFXButton setStyleOnBtn(Tweet t) {
        JFXButton button = new JFXButton();
        button.setPrefHeight(26);
        button.setPrefWidth(110);
        button.setLayoutX(285);
        button.setLayoutY(30);

        if (LoginCtrl.mainDoc.getFollowing().contains(t.getSenderID())) {
            button.getStyleClass().add("unfollow-btn");
            button.setText("UnFollow");
            return button;
        }

        button.getStyleClass().add("follow-btn");
        button.setText("Follow");
        return button;
    }

    private ImageView setImageOfLike(boolean bool, ImageView imageView) {
        if (bool) {
            imageView.setImage(new Image(new File("src/Images/iconfinder_heart_299063.png")
                    .toURI().toString()));
        } else {
            imageView.setImage(new Image(new File("src/Images/heart.png")
                    .toURI().toString()));
        }

        return imageView;
    }

    private void followUnfollowHandler(Tweet t, String textOnBtn, JFXButton btn)
            throws JsonProcessingException {
        if (textOnBtn.equals("Follow")) {
            manageData.addFollowing(t.getSenderID());
            btn.setText("UnFollow");
            btn.getStyleClass().add("unfollow-btn");
        } else if (textOnBtn.equals("UnFollow")) {
            manageData.unFollowUser(t.getSenderID());
            btn.setText("Follow");
            btn.getStyleClass().add("follow-btn");
        }
        prepareMainPage();
    }

    private void like(Tweet t, ImageView i) throws JsonProcessingException {
        manageData.addLiker(t);
        i.setImage(new Image(new File("src/Images/iconfinder_heart_299063.png").toURI().toString()));
    }

    private void unLike(Tweet t, ImageView i) throws JsonProcessingException {
        manageData.deleteLiker(t);
        i.setImage(new Image(new File("src/Images/heart.png").toURI().toString()));
    }

    private void friendsPaneAction(String user) throws JsonProcessingException {
        prepareUserPage(user);
    }

    private double computeSnapshotSimilarity(final Image image1, final Image image2) {
        final int width = (int) image1.getWidth();
        final int height = (int) image1.getHeight();
        final PixelReader reader1 = image1.getPixelReader();
        final PixelReader reader2 = image2.getPixelReader();

        final double nbNonSimilarPixels = IntStream.range(0, width).parallel().
                mapToLong(i -> IntStream.range(0, height).parallel().filter(j -> reader1.getArgb(i, j) != reader2.getArgb(i, j)).count()).sum();

        return 100d - nbNonSimilarPixels / (width * height) * 100d;
    }

    private void checkImageForLike(ImageView i, Tweet t) throws JsonProcessingException {
        Image red = new Image(new File("src/Images/iconfinder_heart_299063.png").toURI().toString());
        Image black = new Image(new File("src/Images/iconfinder_heart_299063.png").toURI().toString());
        if (computeSnapshotSimilarity(i.getImage(), red) == 100.0) {
            unLike(t, i);
        } else {
            like(t, i);
        }

    }


    @FXML
    void deleteAccount(MouseEvent event) {
        mainP.setVisible(false);
        showFriendsPage.setVisible(false);
        showTweetsPage.setVisible(false);
        goToPage.setVisible(false);
        profilePage.setVisible(false);
        changePasswordPage.setVisible(false);
        stackForDeletePage.setVisible(true);
        deleteAccountPage.setVisible(true);
        UserPage.setVisible(false);
        showLikersPane.setVisible(false);

    }

    public boolean delete() throws JsonProcessingException {
        if (LoginCtrl.registry.checkPass(passInDeleteAcc.getText(), LoginCtrl.mainDoc.getPassword())) {
            manageData.deleteAccount(LoginCtrl.mainDoc);
            return true;
        }
        return false;
    }

    @FXML
    void deleteTweet(ActionEvent event, Tweet t) throws JsonProcessingException {
        manageData.deleteTweet(t);
        tweetsPage.getChildren().removeAll(tweetsPage.getChildren());
        PrepareMyTweetsPage();
    }

    @FXML
    void deleteAccountHandler(ActionEvent event) throws JsonProcessingException {
        boolean result = delete();
        if (result) {
            JFXDialogLayout layout = new JFXDialogLayout();
            Text text = new Text("Account successfully deleted.");
            text.setFont(Font.font(14));
            layout.setBody(text);
            layout.setStyle("-fx-background-color: #ffffff");

            JFXDialog dialog = new JFXDialog(stackForDeletePage, layout, JFXDialog.DialogTransition.CENTER);

            JFXButton ok = new JFXButton("Ok");
            ok.setButtonType(JFXButton.ButtonType.RAISED);
            ok.setStyle("-fx-background-color: #7f78d2");
            ok.setTextFill(Color.rgb(34, 31, 59));

            ok.setCenterShape(true);


            ok.setOnAction(e -> {
                dialog.close();
                Main.pStage.close();
            });
            layout.setActions(ok);

            dialog.show();
            deleteAccountPage.setDisable(true);
        } else {
            errorInDeletePage.setText(Values.ErrorString(Values.PASS_INCORRECT));
        }
    }
}