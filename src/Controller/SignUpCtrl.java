package Controller;

import Models.User;
import Models.Values;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import javax.mail.MessagingException;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;


public class SignUpCtrl implements Initializable {
    @FXML
    public TextField username;

    @FXML
    public PasswordField pass;

    @FXML
    private Label ErrorLbl;

    @FXML
    public TextField email;

    @FXML
    public Pane main1;

    @FXML
    public TextField ID;

    @FXML
    public void minimizeHandler(MouseEvent e) {
        Main.pStage.setIconified(true);
    }

    @FXML
    public void closeHandler(MouseEvent event) {
        Main.pStage.close();
    }

    @FXML
    public void undoHandler(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../View/Login.fxml"));
        main1.getChildren().setAll(root);
    }

    @FXML
    public void creatHandler(ActionEvent event) throws IOException, MessagingException, NoSuchAlgorithmException {
        if (email.getText().equals("") || username.getText().equals("") || pass.getText().equals("")) {
            ErrorLbl.setText(Values.ErrorString(Values.FILL_FIELDS));
        } else if (pass.getText().length() != 4) {
            ErrorLbl.setText(Values.ErrorString(Values.PASS_4_CHAR));
        } else {
            User u = new User(ID.getText(), email.getText(), username.getText(), pass.getText());
            EnterCodeCtrl enterCodeCtrl = new EnterCodeCtrl();
            enterCodeCtrl.setUserForCode(u);

            if (LoginCtrl.registry.signUp(u)) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../View/EnterCode.fxml"));
                Parent root = loader.load();
                enterCodeCtrl = loader.getController();
                email.setStyle("-fx-underline: true");
                enterCodeCtrl.description.setText(enterCodeCtrl.description.getText() + email.getText());
                main1.getChildren().setAll(root);
            } else if (LoginCtrl.registry.flag && !LoginCtrl.registry.signUp(u)) {
                ErrorLbl.setText(Values.ErrorString(Values.EMAIL_USED));
            } else {
                ErrorLbl.setText(Values.ErrorString(Values.USERNAME_USED));
            }
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ErrorLbl.setFont(new Font(13));
        ErrorLbl.setTextFill(Color.rgb(144, 8, 8));

        pass.textProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue == null) {
                return;
            }

            if (newValue.length() > 4) {
                pass.setText(oldValue);
            } else {
                pass.setText(newValue);
            }
        });

    }
}
