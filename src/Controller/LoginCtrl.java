package Controller;

import Models.User;
import Models.Values;
import DBPackage.Registry;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginCtrl implements Initializable {
    private MainPageCtrl mainPageCtrl;
    public static Registry registry;
    public static User mainDoc;


    public LoginCtrl() {
        this.registry = new Registry();
    }

    @FXML
    private Label ErrorLbl;

    @FXML
    private TextField ID;

    @FXML
    private PasswordField password;

    @FXML
    private Pane pane;


    @FXML
    private void minimizeHandler(MouseEvent e) {
        Main.pStage.setIconified(true);
    }

    @FXML
    private void closeHandler(MouseEvent event) {
        Main.pStage.close();
    }

    @FXML
    private void forgotPassHandler(MouseEvent e) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../View/PasswordPages.fxml"));
        Parent root = loader.load();
        PasswordPagesCtrl ctrl = loader.getController();
        ctrl.resetPasswordPage.setVisible(true);
        pane.getChildren().setAll(root);
    }

    @FXML
    private void loginHandler(ActionEvent event) throws Exception {
        boolean result = registry.LogIn(ID.getText(), password.getText());

        if (!result) {
            ErrorLbl.setText(Values.ErrorString(Values.USER_PASS_INCORRECT));
        } else {
            mainDoc = registry.getUserFromDatabase(ID.getText());

            FXMLLoader loader = new FXMLLoader(getClass().getResource("../View/MainPage.fxml"));
            Parent root = loader.load();
            mainPageCtrl = loader.getController();
            mainPageCtrl.mainP.setVisible(true);
            pane.getChildren().setAll(root);

            mainPageCtrl.startPage();

            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Main.xOffset = event.getSceneX();
                    Main.yOffset = event.getSceneY();
                }
            });

            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Main.pStage.setX(event.getScreenX() - Main.xOffset);
                    Main.pStage.setY(event.getScreenY() - Main.yOffset);
                }
            });
        }
    }

    @FXML
    private void signUpHandler(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../View/SignUp.fxml"));
        pane.getChildren().setAll(root);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ErrorLbl.setTextFill(Color.rgb(144, 8, 8));
    }


}
