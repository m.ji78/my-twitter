package Controller;

import Models.User;
import Models.Values;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class EnterCodeCtrl implements Initializable {
    public static User userInEnterCode;

    @FXML
    private Pane main2;

    @FXML
    private StackPane stack;

    @FXML
    public Label errorLbl;

    @FXML
    public Label description;

    @FXML
    private TextField codeField;

    public void setUserForCode(User user) {
        userInEnterCode = user;
    }

    @FXML
    public void minimizeHandler(MouseEvent e) {
        Main.pStage.setIconified(true);
    }

    @FXML
    public void closeHandler(MouseEvent event) {
        Main.pStage.close();
    }

    @FXML
    public void undoHandler(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../View/SignUp.fxml"));
        main2.getChildren().setAll(root);
    }

    @FXML
    public void EnterBtnAction(ActionEvent event) throws InterruptedException, IOException {

        if (codeField.getText().equals("")) {
            errorLbl.setText(Values.ErrorString(Values.INCORRECT_CODE));
        } else if (LoginCtrl.registry.checkEnterCode(userInEnterCode, codeField.getText())) {

            JFXDialogLayout layout = new JFXDialogLayout();
            Text text = new Text(Values.ErrorString(Values.SIGN_UP));
            text.setFont(Font.font(14));
            layout.setBody(text);
            layout.setStyle("-fx-background-color: #94ACD9");

            JFXDialog dialog = new JFXDialog(stack, layout, JFXDialog.DialogTransition.CENTER);

            JFXButton ok = new JFXButton("Ok");
            ok.setButtonType(JFXButton.ButtonType.RAISED);
            ok.setStyle("-fx-background-color: #221f3b");
            ok.setTextFill(Color.rgb(255, 226, 255));


            ok.setOnAction(e -> {
                dialog.close();
                Main.pStage.close();
            });

            JFXButton back = new JFXButton("Back To Login Page");
            back.setButtonType(JFXButton.ButtonType.RAISED);
            back.setStyle("-fx-background-color: #221f3b");
            back.setTextFill(Color.rgb(255, 226, 255));

            layout.setActions(ok, back);

            back.setOnAction(e -> {
                Parent root = null;
                try {
                    root = FXMLLoader.load(getClass().getResource("../View/Login.fxml"));
                    main2.setDisable(false);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                dialog.close();
                main2.getChildren().setAll(root);
                System.out.println("EnterCode page loaded");
            });

            dialog.show();
            main2.setDisable(true);
        } else if (!LoginCtrl.registry.checkEnterCode(userInEnterCode, codeField.getText())) {
            errorLbl.setText(Values.ErrorString(Values.INCORRECT_CODE));
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        errorLbl.setFont(new Font(13));
        errorLbl.setTextFill(Color.rgb(144, 8, 8));

        description.setText("We'll send a verification code to your Email address.\n" +
                "please enter it below.\n" +
                "Your Email address is :\n");

        codeField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                                String oldValue, String newValue) {

                if (newValue == null) {
                    return;
                }
                if (newValue.length() > 4) {
                    codeField.setText(oldValue);
                } else {
                    codeField.setText(newValue);
                }
            }
        });
    }
}
